const express = require("express");
const bodyParser = require("body-parser");
const axios = require("axios");
const { JSDOM } = require("jsdom");
const TurndownService = require("turndown");
require("dotenv").config(); // Load environment variables from .env file

const app = express();
const PORT = process.env.PORT || 3000; // Use port from environment variables or default to 3000
const DISCORD_WEBHOOK_URL = process.env.DISCORD_WEBHOOK_URL; // Use Discord webhook URL from environment variables
const TASK_URL_BASE = process.env.TASK_URL_BASE; // Use task URL base from environment variables
const TIMEOUT_MINUTES = parseInt(process.env.TIMEOUT_MINUTES, 10) || 5; // Use timeout in minutes from environment variables or default to 5 minutes

if (!DISCORD_WEBHOOK_URL) {
  console.error("Discord webhook URL is not set. Please set it in the .env file.");
  process.exit(1);
}

if (!TASK_URL_BASE) {
  console.error("Task URL base is not set. Please set it in the .env file.");
  process.exit(1);
}

// Middleware to parse JSON payloads
app.use(bodyParser.json());

// Cache to keep track of the last update time and latest payload by task ID
const taskUpdateCache = {};

// Helper function to send payload to Discord webhook
const sendToDiscord = async (payload) => {
  const eventNameWithSpaces = payload.event_name.replace(".", " ");
  const username = payload.data.doer.username;
  const board = payload.board;
  const taskTitle = payload.data.task.title;
  let taskDescription = payload.data.task.description;
  const dueDate = payload.data.task.due_date !== "0001-01-01T00:00:00Z" ? payload.data.task.due_date : null;
  const labels = payload.data.task.labels;
  const taskId = payload.data.task.id;
  const taskUrl = `${TASK_URL_BASE}${taskId}`;

  // Convert the HTML description to Markdown using Turndown
  const turndownService = new TurndownService();
  taskDescription = turndownService.turndown(taskDescription);

  const embedTitle = `${eventNameWithSpaces} by ${username} in ${board}`;
  let embedDescription = `**${taskTitle}**\n${taskDescription}\n\n[View Task](${taskUrl})`;

  if (dueDate) {
    embedDescription += `\n\n**Due Date:** ${dueDate}`;
  }

  if (labels && labels.length > 0) {
    const labelTitles = labels.map((label) => label.title);
    embedDescription += `\n\n**Labels:** ${labelTitles.join(", ")}`;
  }

  const discordPayload = {
    embeds: [
      {
        title: embedTitle,
        description: embedDescription,
        color: 3447003, // You can choose any color you like
      },
    ],
  };

  try {
    await axios.post(DISCORD_WEBHOOK_URL, discordPayload);
    console.log("Payload sent to Discord successfully");
  } catch (error) {
    console.error("Error sending payload to Discord:", error);
  }
};

// Function to handle incoming payloads
const handlePayload = (payload) => {
  const taskId = payload.data.task.id;

  // Check if there is already a timeout set for this task
  if (taskUpdateCache[taskId]) {
    clearTimeout(taskUpdateCache[taskId].timeout);
  }

  // Set a new timeout to send the webhook after the configured timeout duration
  taskUpdateCache[taskId] = {
    payload: payload,
    timeout: setTimeout(() => {
      sendToDiscord(payload);
      delete taskUpdateCache[taskId];
    }, TIMEOUT_MINUTES * 60 * 1000), // Convert minutes to milliseconds
  };
};

// Endpoint to receive webhook payloads
app.post("/webhook", (req, res) => {
  const boardName = req.query.board;
  const payload = req.body;

  if (!boardName) {
    return res.status(400).send("Missing board query parameter");
  }

  // Add board name to the root of the payload
  const enhancedPayload = {
    board: boardName,
    ...payload,
  };

  console.log("Received payload:", enhancedPayload);

  handlePayload(enhancedPayload);

  res.status(200).send("Payload received");
});

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});
